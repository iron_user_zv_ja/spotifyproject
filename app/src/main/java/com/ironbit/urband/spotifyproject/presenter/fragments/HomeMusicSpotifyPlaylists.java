package com.ironbit.urband.spotifyproject.presenter.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ironbit.urband.spotifyproject.Application;
import com.ironbit.urband.spotifyproject.R;
import com.ironbit.urband.spotifyproject.model.interfaces.SpotifyControls;
import com.ironbit.urband.spotifyproject.model.preferences.SpotifyCredentials;
import com.ironbit.urband.spotifyproject.presenter.activities.HomeMusicSpotifySongs;
import com.ironbit.urband.spotifyproject.presenter.helpers.SpotifyHelper;
import com.ironbit.urband.spotifyproject.view.adapters.RecyclerPlaylists;

import java.util.List;

import kaaes.spotify.webapi.android.models.PlaylistSimple;
import kaaes.spotify.webapi.android.models.Track;

/**
 * Created by Alejandro on 11/02/2016.
 */
public class HomeMusicSpotifyPlaylists extends Fragment implements SpotifyControls.AddPlaylist {

    private View rootView;

    private RecyclerView recyclerPlaylists;
    private RecyclerPlaylists adapterPlaylist;
    private LinearLayoutManager llmPlayList;

    //private SpotifyControls.ActionListener mActionListener;
    private SpotifyCredentials spotifyCredentials;

    @Override
    public void onResume() {
        super.onResume();
        //reset();
        //mActionListener.getPlaylists();
        //Application.getInstance().getActionListener().getPlaylists();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.home_fragment_music_spotify_playlists, container, false);

        initResources();
        //mActionListener.getPlaylists();
        setSpotifyAdapter();

        return rootView;
    }

    private void initResources() {

        spotifyCredentials = new SpotifyCredentials();

        recyclerPlaylists = (RecyclerView) rootView.findViewById(R.id.music_spotify_list_playlists);
        recyclerPlaylists.setHasFixedSize(true);

        llmPlayList = new LinearLayoutManager(getActivity());
        llmPlayList.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerPlaylists.setLayoutManager(llmPlayList);

        //mActionListener = new SpotifyHelper(getActivity(), this);
        //mActionListener.init(spotifyCredentials.getToken());
        //Application.getInstance().initSpotify();
        Application.getInstance().getActionListener().setSpotifyControlsAddPlaylist(this);
        Application.getInstance().getActionListener().getPlaylists();
    }

    private void setSpotifyAdapter(){

        adapterPlaylist = new RecyclerPlaylists(getActivity(), R.layout.home_fragment_music_spotify_playlists_row);
        adapterPlaylist.setOnItemRecyclerClickListener(new RecyclerPlaylists.OnItemRecyclerClickListener() {
            @Override
            public void onItemRecyclerClick(View view, PlaylistSimple item) {

                Intent spotifySong = new Intent(getActivity(), HomeMusicSpotifySongs.class);
                spotifySong.putExtra("idPlaylist", item.id);
                startActivity(spotifySong);
            }
        });
        recyclerPlaylists.setAdapter(adapterPlaylist);
    }

    @Override
    public void reset() {
        adapterPlaylist.clearDataSpotify();
    }

    @Override
    public void addPlayList(List<PlaylistSimple> playlist) {
        adapterPlaylist.addDataSpotify(playlist);
    }

}
