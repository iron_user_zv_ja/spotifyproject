package com.ironbit.urband.spotifyproject.presenter.helpers;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.ironbit.urband.spotifyproject.model.interfaces.SpotifyControls;
import com.ironbit.urband.spotifyproject.model.interfaces.SpotifyListener;
import com.ironbit.urband.spotifyproject.model.preferences.SpotifyCredentials;
import com.ironbit.urband.spotifyproject.view.utils.SpotifyCallbackUtils;
import com.spotify.sdk.android.player.Config;
import com.spotify.sdk.android.player.PlayerStateCallback;
import com.spotify.sdk.android.player.Spotify;

import com.spotify.sdk.android.player.ConnectionStateCallback;
import com.spotify.sdk.android.player.Player;
import com.spotify.sdk.android.player.PlayerNotificationCallback;
import com.spotify.sdk.android.player.PlayerState;

import java.util.List;

import kaaes.spotify.webapi.android.SpotifyApi;
import kaaes.spotify.webapi.android.models.PlaylistSimple;
import kaaes.spotify.webapi.android.models.Track;

public class SpotifyHelper implements SpotifyControls.ActionListener, PlayerNotificationCallback, ConnectionStateCallback {

    private static final String TAG = SpotifyHelper.class.getSimpleName();

    private final Context mContext;
    private final SpotifyControls.AddItem mAddItem;
    private SpotifyControls.AddUser mAddUser;

    private SpotifyCallbackUtils mSpotifyCallbackUtils;
    private SpotifyListener.CompleteListenerUser listenerUser;
    private SpotifyListener.CompleteListenerListSongs listenerSongs;
    private SpotifyListener.CompleteListenerPlayList listenerPlaylists;

    private Player mPlayer;
    private SpotifyCredentials spotifyCredentials;

    private String currentTrack = "";
    private boolean playerIsPlaying = false;

    public SpotifyHelper(Context context, SpotifyControls.AddItem addItem) {
        mContext = context;
        mAddItem = addItem;
        spotifyCredentials = new SpotifyCredentials();
    }

    public SpotifyHelper(Context context, SpotifyControls.AddItem addItem, SpotifyControls.AddUser addUser) {
        mContext = context;
        mAddItem = addItem;
        mAddUser = addUser;
        spotifyCredentials = new SpotifyCredentials();
    }

    @Override
    public void init(String accessToken) {
        //logMessage("Api Client created");
        Log.i("SpotifyHelper", "Api Client created");
        SpotifyApi spotifyApi = new SpotifyApi();

        if (accessToken != null) {
            spotifyApi.setAccessToken(accessToken);
        } else {
            //logError("No valid access token");
            Log.i("SpotifyHelper", "No valid access token");
        }

        mSpotifyCallbackUtils = new SpotifyCallbackUtils(spotifyApi.getService());

    }

    @Override
    public void selectTrack(final Track item, final List<String> uris) {

        initPlayer();

        final String previewUrl = item.uri;

        mPlayer.getPlayerState(new PlayerStateCallback() {
            @Override
            public void onPlayerState(PlayerState playerState) {

                currentTrack = playerState.trackUri;
                playerIsPlaying = playerState.playing;

                if (item.uri == null) {
                    logMessage("Track doesn't have a uri correct");
                    return;
                }

                if (mPlayer == null) return;

                if (currentTrack == null || !currentTrack.equals(previewUrl)) {
                    //mPlayer.play(previewUrl);
                    mPlayer.play(uris);
                } else if (playerIsPlaying) {
                    mPlayer.pause();
                } else {
                    mPlayer.resume();
                }

            }
        });

    }

    @Override
    public void skipToNextTrack() {
        mPlayer.skipToNext();
    }

    @Override
    public void skipToPreviousTrack() {
        mPlayer.skipToPrevious();
    }

    @Override
    public void destroy() {
    }

    @Override
    public void getUser() {
        listenerUser = new SpotifyListener.CompleteListenerUser(){

            @Override
            public void onComplete(String user) {
                mAddUser.addUser(user);
            }

            @Override
            public void onError(Throwable error) {

            }
        };
        mSpotifyCallbackUtils.getNameUser(listenerUser);
    }

    @Override
    public void getPlaylists() {
        listenerPlaylists = new SpotifyListener.CompleteListenerPlayList(){
            @Override
            public void onComplete(List<PlaylistSimple> items) {
                //mAddItem.addPlayList(items);
            }

            @Override
            public void onError(Throwable error) {

            }
        };
        mSpotifyCallbackUtils.getPlaylists(listenerPlaylists);
    }

    @Override
    public void getListSongs(String idPlaylist) {
        listenerSongs = new SpotifyListener.CompleteListenerListSongs() {
            @Override
            public void onComplete(List<Track> items) {
                mAddItem.addData(items);
            }

            @Override
            public void onError(Throwable error) {
                //logError(error.getMessage());
                Log.i("SpotifyHelper", error.getMessage());
            }
        };
        mSpotifyCallbackUtils.getListSongs(listenerSongs, idPlaylist);
    }

    @Override
    public void setSpotifyControlsAddPlaylist(SpotifyControls.AddPlaylist addPlaylist) {

    }

    @Override
    public void setSpotifyControlsAddItem(SpotifyControls.AddItem addItem) {

    }

    @Override
    public void setSpotifyControlsAddUser(SpotifyControls.AddUser addUser) {

    }

    @Override
    public void resume() {
    }

    @Override
    public void pause() {
    }


    private void initPlayer(){
        /* PLAYER SE DEBE IMPLEMENTAR EN SERVICIO PARA EVITAR CRASHEO.
         *      CUANDO RECONSTRUYE EL LISTENER CRASHEA YA QUE ES INICIALIZADO EN FRAGMENT
         *      NECESARIO EL USO DE UN SERVICIO.
         */
        Config playerConfig=new Config(mContext, spotifyCredentials.getToken(), SpotifyCredentials.CLIENT_ID);
        mPlayer=Spotify.getPlayer(playerConfig,this,new Player.InitializationObserver(){
                    @Override public void onInitialized(    Player player){
                        mPlayer.addConnectionStateCallback(SpotifyHelper.this);
                        mPlayer.addPlayerNotificationCallback(SpotifyHelper.this);
                    }
                    @Override public void onError(    Throwable throwable){
                        Log.e("MainActivity","Could not initialize player: " + throwable.getMessage());
                    }
                }
        );
    }


    private void logError(String msg) {
        Toast.makeText(mContext, "Error: " + msg, Toast.LENGTH_SHORT).show();
        Log.e(TAG, msg);
    }

    private void logMessage(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
        Log.d(TAG, msg);
    }

    @Override
    public void onLoggedIn() {
    }

    @Override
    public void onLoggedOut() {

    }

    @Override
    public void onLoginFailed(Throwable throwable) {

    }

    @Override
    public void onTemporaryError() {

    }

    @Override
    public void onConnectionMessage(String s) {

    }

    @Override
    public void onPlaybackEvent(EventType eventType, PlayerState playerState) {

    }

    @Override
    public void onPlaybackError(ErrorType errorType, String s) {

    }


}

