package com.ironbit.urband.spotifyproject.model.interfaces;

import java.util.List;

import kaaes.spotify.webapi.android.models.PlaylistSimple;
import kaaes.spotify.webapi.android.models.Track;

/**
 * Created by Alejandro on 10/02/2016.
 */
public class SpotifyListener {

    public interface CompleteListenerUser{
        void onComplete(String user);
        void onError(Throwable error);
    }

    public interface CompleteListenerListSongs {
        void onComplete(List<Track> items);
        void onError(Throwable error);
    }

    public interface CompleteListenerPlayList{
        void onComplete(List<PlaylistSimple> items);
        void onError(Throwable error);
    }
}
