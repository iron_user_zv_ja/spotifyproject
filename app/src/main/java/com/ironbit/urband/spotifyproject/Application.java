package com.ironbit.urband.spotifyproject;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

import com.ironbit.urband.spotifyproject.model.interfaces.SpotifyControls;
import com.ironbit.urband.spotifyproject.model.preferences.SpotifyCredentials;
import com.ironbit.urband.spotifyproject.presenter.services.PlayerService;
import com.ironbit.urband.spotifyproject.view.utils.SpotifyCallbackUtils;
import com.spotify.sdk.android.player.Config;
import com.spotify.sdk.android.player.ConnectionStateCallback;
import com.spotify.sdk.android.player.Player;
import com.spotify.sdk.android.player.PlayerNotificationCallback;
import com.spotify.sdk.android.player.PlayerState;
import com.spotify.sdk.android.player.Spotify;

import kaaes.spotify.webapi.android.SpotifyApi;

/**
 * Created by Alejandro on 10/02/2016.
 */
public class Application extends android.app.Application{

    private static Application singleton;

    public static Application getInstance() {
        return singleton;
    }

    private ServiceConnection mServiceConnection;
    private SpotifyCallbackUtils spotifyCallbackUtils;
    private SpotifyControls.ActionListener mActionListener;
    private SpotifyCredentials spotifyCredentials;
    private Player mPlayer;


    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;

        mServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                //mPlayer = ((PlayerService.PlayerBinder) service).getService();
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                //mPlayer = null;
            }
        };

        initSpotify();
    }

    public ServiceConnection getServiceConnectionSpotify() {
        return mServiceConnection;
    }

    public SpotifyCallbackUtils getSpotifyCallbackUtils(){
        return spotifyCallbackUtils;
    }

    public SpotifyControls.ActionListener getActionListener(){
        return mActionListener;
    }

    public void initSpotify() {
        SpotifyCredentials spotifyCredentials = new SpotifyCredentials();
        SpotifyApi spotifyApi = new SpotifyApi();
        if (spotifyCredentials.getToken() != null) {
            spotifyApi.setAccessToken(spotifyCredentials.getToken());
        } else {
            Log.e("Application", "debug: No valid access token");
        }

        spotifyCallbackUtils = new SpotifyCallbackUtils(spotifyApi.getService());
        getInstance().bindService(PlayerService.getIntent(getInstance()), mServiceConnection, Activity.BIND_AUTO_CREATE);
        mActionListener = new PlayerService();
    }

    public void initPlayerSpotify(){
        spotifyCredentials = new SpotifyCredentials();
        Config playerConfig=new Config(Application.getInstance(), spotifyCredentials.getToken(), SpotifyCredentials.CLIENT_ID);
        mPlayer= Spotify.getPlayer(playerConfig, this, new Player.InitializationObserver() {
                    @Override
                    public void onInitialized(Player player) {
                        mPlayer.addConnectionStateCallback(getConnectionStateCallback());
                        mPlayer.addPlayerNotificationCallback(getPlayerNotificationCallback());
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        Log.e("MainActivity", "Could not initialize player: " + throwable.getMessage());
                    }
                }
        );
    }

    public Player getPlayerSpotify(){
        return mPlayer;
    }

    public PlayerNotificationCallback getPlayerNotificationCallback(){
        return new PlayerNotificationCallback() {
            @Override
            public void onPlaybackEvent(EventType eventType, PlayerState playerState) {

            }

            @Override
            public void onPlaybackError(ErrorType errorType, String s) {

            }
        };
    }

    public ConnectionStateCallback getConnectionStateCallback(){
        return new ConnectionStateCallback() {
            @Override
            public void onLoggedIn() {

            }

            @Override
            public void onLoggedOut() {

            }

            @Override
            public void onLoginFailed(Throwable throwable) {

            }

            @Override
            public void onTemporaryError() {

            }

            @Override
            public void onConnectionMessage(String s) {

            }
        };
    }

    public boolean MediaPlayerServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if ("com.ironbit.urband.spotifyproject.presenter.services.PlayerService".equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public void bindToServiceSpotify() {
        Intent intent = new Intent(Application.getInstance().getApplicationContext(), PlayerService.class);
        if (MediaPlayerServiceRunning()) {
            // Bind to LocalService
            Log.i("debu", "MediaPlayerService is Running");
            Application.getInstance().getApplicationContext().bindService(intent, getServiceConnectionSpotify(), Context.BIND_AUTO_CREATE);
        } else {
            Log.i("debug", "MediaPlayerService is not Running");
            Application.getInstance().getApplicationContext().startService(intent);
            Application.getInstance().getApplicationContext().bindService(intent, getServiceConnectionSpotify(), Context.BIND_AUTO_CREATE);
        }

    }

    public void unbindFromServiceSpotify(){
        unbindService(getServiceConnectionSpotify());
    }

    @Override
    public ComponentName startService(Intent service) {
        return super.startService(service);
    }
}

