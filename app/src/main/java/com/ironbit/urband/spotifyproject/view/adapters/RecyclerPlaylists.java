package com.ironbit.urband.spotifyproject.view.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import com.ironbit.urband.spotifyproject.R;

import java.util.ArrayList;
import java.util.List;

import kaaes.spotify.webapi.android.models.PlaylistSimple;
import kaaes.spotify.webapi.android.models.Image;

/**
 * Created by Alejandro on 10/02/2016.
 */
public class RecyclerPlaylists extends RecyclerView.Adapter<RecyclerPlaylists.Holder> {

    private final List<PlaylistSimple> data = new ArrayList<>();
    private final Context context;
    private int resource;
    private OnItemRecyclerClickListener listener;


    public interface OnItemRecyclerClickListener {
        void onItemRecyclerClick(View view, PlaylistSimple item);
    }

    public void setOnItemRecyclerClickListener(OnItemRecyclerClickListener listener){
        this.listener = listener;
    }

    public RecyclerPlaylists(Context context, int resource) {
        this.context = context;
        this.resource = resource;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(resource, parent, false);
        Holder pvh = new Holder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        PlaylistSimple item = data.get(position);

        holder.namePlaylist.setText(item.name);

        if(item.images.size() > 0){
            Image image = item.images.get(0);
            if (image != null) {
                Picasso.with(context).load(image.url).into(holder.imagePlaylist);
            }
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void clearDataSpotify() {
        data.clear();
    }

    public void addDataSpotify(List<PlaylistSimple> items) {
        data.addAll(items);
        notifyDataSetChanged();
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public final TextView namePlaylist;
        public final TextView subtitle;
        public final ImageView imagePlaylist;

        public Holder(View itemView) {
            super(itemView);
            namePlaylist = (TextView) itemView.findViewById(R.id.music_spotify_playlists_row_text_name);
            subtitle = (TextView) itemView.findViewById(R.id.music_spotify_playlists_row_text_subtitle);
            imagePlaylist = (ImageView) itemView.findViewById(R.id.music_spotify_playlists_row_image);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            notifyItemChanged(getLayoutPosition());
            listener.onItemRecyclerClick(v, data.get(getAdapterPosition()));
        }
    }

}
