package com.ironbit.urband.spotifyproject.presenter.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.ironbit.urband.spotifyproject.Application;
import com.ironbit.urband.spotifyproject.model.interfaces.SpotifyControls;
import com.ironbit.urband.spotifyproject.model.preferences.SpotifyCredentials;
import com.ironbit.urband.spotifyproject.R;
import com.ironbit.urband.spotifyproject.presenter.fragments.HomeMusicSpotifyLogin;
import com.ironbit.urband.spotifyproject.presenter.fragments.HomeMusicSpotifyPlaylists;
import com.ironbit.urband.spotifyproject.presenter.services.PlayerService;
import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationResponse;

import java.util.List;
import java.util.concurrent.TimeUnit;

import kaaes.spotify.webapi.android.models.PlaylistSimple;
import kaaes.spotify.webapi.android.models.Track;

/**
 * Created by Alex on 17/01/16.
 */
public class HomeMusicSpotify extends AppCompatActivity  implements SpotifyControls.AddUser{

    private static final String TAG = HomeMusicSpotify.class.getSimpleName();

    @SuppressWarnings("SpellCheckingInspection")
    private final int REQUEST_CODE = 1337;
    private String token = null;
    private SpotifyCredentials spotifyCredentials;
    private Bundle savedInstanceState = null;

    //public static SpotifyControls.ActionListener mActionListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity_music_spotify);

        initResources();
        this.savedInstanceState = savedInstanceState;

        if (token == null) {
            addFragment(new HomeMusicSpotifyLogin());
        }
        else{
            addFragment(new HomeMusicSpotifyPlaylists());
        }

    }

    private void initResources(){
        spotifyCredentials = new SpotifyCredentials();
        token = spotifyCredentials.getToken();

        //mActionListener = new SpotifyHelper(this, this, this);
        //mActionListener.init(token);
        Application.getInstance().initSpotify();
        Application.getInstance().initPlayerSpotify();
        Application.getInstance().getActionListener().setSpotifyControlsAddUser(this);
    }

    private void addFragment(Fragment fragment){
        getSupportFragmentManager().beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(R.id.music_spotify_frame_container, fragment)
                .commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        //HomeMusicSpotifyLogin fragmentLogin = new HomeMusicSpotifyLogin();
        //fragmentLogin.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE) {
            AuthenticationResponse response = AuthenticationClient.getResponse(resultCode, intent);
            switch (response.getType()) {
                case TOKEN:
                    spotifyCredentials.setToken(this, response.getAccessToken(), response.getExpiresIn(), TimeUnit.SECONDS);
                    initResources();
                    Application.getInstance().getActionListener().getUser();
                    //replaceFragment(new HomeMusicSpotifyPlaylists());
                    break;
                case ERROR:
                    Log.e(TAG, response.getError());
                    break;
                default:
                    Log.e(TAG, response.getType().toString());
            }
        }

    }

    @Override
    public void addUser(String idUser) {
        Toast.makeText(HomeMusicSpotify.this, "id user: " + idUser, Toast.LENGTH_SHORT).show();
        addFragment(new HomeMusicSpotifyPlaylists());
    }

    @Override
    public void onPause() {
        super.onPause();
        Application.getInstance().getActionListener().pause();
        Application.getInstance().startService(PlayerService.getIntent(Application.getInstance()));
        Application.getInstance().bindToServiceSpotify();
    }

    @Override
    public void onResume() {
        super.onResume();
        Application.getInstance().getActionListener().resume();
        Application.getInstance().stopService(PlayerService.getIntent(Application.getInstance()));
    }

    @Override
    public void onDestroy() {
        Application.getInstance().getActionListener().destroy();
        Application.getInstance().unbindFromServiceSpotify();
        Application.getInstance().stopService(PlayerService.getIntent(Application.getInstance()));
        super.onDestroy();
    }

}
