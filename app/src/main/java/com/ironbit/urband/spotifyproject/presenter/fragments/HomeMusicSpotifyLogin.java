package com.ironbit.urband.spotifyproject.presenter.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.ironbit.urband.spotifyproject.Application;
import com.ironbit.urband.spotifyproject.R;
import com.ironbit.urband.spotifyproject.model.preferences.SpotifyCredentials;
import com.ironbit.urband.spotifyproject.presenter.activities.HomeMusicSpotify;
import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;

/**
 * Created by Alejandro on 13/01/2016.
 */
public class HomeMusicSpotifyLogin extends Fragment {

    private static final String TAG = HomeMusicSpotifyLogin.class.getSimpleName();

    @SuppressWarnings("SpellCheckingInspection")
    private final String REDIRECT_URI = "android://callback";
    private final int REQUEST_CODE = 1337;

    private SpotifyCredentials spotifyCredentials;

    private Button btn_login;
    private View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.home_fragment_music_spotify_login, container, false);

        initResources();

        return rootView;
    }


    private void initResources(){
        spotifyCredentials = new SpotifyCredentials();
        Application.getInstance().initSpotify();
        Application.getInstance().getActionListener().init(spotifyCredentials.getToken());

        btn_login = (Button) rootView.findViewById(R.id.music_spotify_btn_login);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initSessionSpotify();
            }
        });
    }

    private void initSessionSpotify(){
        final AuthenticationRequest request = new AuthenticationRequest.Builder(SpotifyCredentials.CLIENT_ID, AuthenticationResponse.Type.TOKEN, REDIRECT_URI)
                .setScopes(new String[]{"playlist-read", "user-read-email", "user-read-birthdate", "streaming"})
                .build();

        AuthenticationClient.openLoginActivity(getActivity(), REQUEST_CODE, request);
    }

}
