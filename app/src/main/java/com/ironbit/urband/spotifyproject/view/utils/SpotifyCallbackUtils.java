package com.ironbit.urband.spotifyproject.view.utils;

import android.util.Log;

import com.ironbit.urband.spotifyproject.Application;
import com.ironbit.urband.spotifyproject.model.interfaces.SpotifyListener;
import com.ironbit.urband.spotifyproject.model.preferences.SpotifyCredentials;

import java.util.ArrayList;
import java.util.List;

import kaaes.spotify.webapi.android.SpotifyCallback;
import kaaes.spotify.webapi.android.SpotifyError;
import kaaes.spotify.webapi.android.SpotifyService;
import kaaes.spotify.webapi.android.models.Pager;
import kaaes.spotify.webapi.android.models.PlaylistSimple;
import kaaes.spotify.webapi.android.models.PlaylistTrack;
import kaaes.spotify.webapi.android.models.Track;
import kaaes.spotify.webapi.android.models.UserPrivate;
import retrofit.client.Response;


public class SpotifyCallbackUtils {

    private final SpotifyService mSpotifyApi;
    private SpotifyCredentials spotifyCredentials;


    public SpotifyCallbackUtils(SpotifyService spotifyApi) {
        mSpotifyApi = spotifyApi;
        spotifyCredentials = new SpotifyCredentials();
    }

    public void getNameUser(final SpotifyListener.CompleteListenerUser listener){
        mSpotifyApi.getMe(new SpotifyCallback<UserPrivate>() {
            @Override
            public void failure(SpotifyError spotifyError) {
            }

            @Override
            public void success(UserPrivate userPrivate, Response response) {
                Log.i("SpotifyCallbackUtils:", "e-mail: " + userPrivate.email + " id: " + userPrivate.id);
                spotifyCredentials.setSpotifyUser(userPrivate.id);
                listener.onComplete(userPrivate.id);
            }
        });
    }

    public void getPlaylists(final SpotifyListener.CompleteListenerPlayList listener){
        mSpotifyApi.getPlaylists(spotifyCredentials.getUserSpotify(), new SpotifyCallback<Pager<PlaylistSimple>>() {
            @Override
            public void success(Pager<PlaylistSimple> playlistSimplePager, Response response) {

                List<PlaylistSimple> playlists = new ArrayList<>();
                for (PlaylistSimple item : playlistSimplePager.items) {
                    Log.i("debug: idPlaylist: ", item.id +" : "+ item.name);
                    playlists.add(item);
                }
                listener.onComplete(playlists);

            }

            @Override
            public void failure(SpotifyError spotifyError) {
            }
        });
    }


    public void getListSongs(final SpotifyListener.CompleteListenerListSongs listener, String idPlaylist){
        mSpotifyApi.getPlaylistTracks(spotifyCredentials.getUserSpotify(), idPlaylist, new SpotifyCallback<Pager<PlaylistTrack>>() {
            @Override
            public void failure(SpotifyError spotifyError) {
            }

            @Override
            public void success(Pager<PlaylistTrack> playlistTrackPager, Response response) {
                List<PlaylistTrack> item0 = playlistTrackPager.items;

                List<PlaylistTrack> qwe = playlistTrackPager.items;
                List<Track> tracks = new ArrayList<>();

                for(PlaylistTrack item : qwe){
                    Track asd = item.track;
                    Log.i("debug: listSong track: ", String.valueOf(item.track));
                    tracks.add(asd);
                }
                listener.onComplete(tracks);
            }
        });
    }

}
