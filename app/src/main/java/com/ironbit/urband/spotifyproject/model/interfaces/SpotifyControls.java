package com.ironbit.urband.spotifyproject.model.interfaces;

import java.util.List;

import kaaes.spotify.webapi.android.models.PlaylistSimple;
import kaaes.spotify.webapi.android.models.Track;

/**
 * Created by Alex on 17/01/16.
 */
public class SpotifyControls {

    public interface AddPlaylist {
        void reset();
        void addPlayList(List<PlaylistSimple> playlist);
    }

    public interface AddItem {
        void reset();
        void addData(List<Track> items);
    }

    public interface AddUser{
        void addUser(String idUser);
    }

    public interface ActionListener {

        void init(String token);

        void selectTrack(Track item, List<String> uris);

        void skipToNextTrack();

        void skipToPreviousTrack();

        void resume();

        void pause();

        void destroy();

        void getUser();

        void getPlaylists();

        void getListSongs(String idPlaylist);

        void setSpotifyControlsAddPlaylist(SpotifyControls.AddPlaylist addPlaylist);
        void setSpotifyControlsAddItem(SpotifyControls.AddItem addItem);
        void setSpotifyControlsAddUser(SpotifyControls.AddUser addUser);
    }
}
