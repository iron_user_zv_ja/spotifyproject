package com.ironbit.urband.spotifyproject.model.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.ironbit.urband.spotifyproject.Application;

import java.util.concurrent.TimeUnit;

public class SpotifyCredentials {

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @SuppressWarnings("SpellCheckingInspection")
    public static final String CLIENT_ID = "9d59b8a54b1841ed9dee0718e7591863";
    //GENERATE ->  SPOTIFY DEVELOPER (CLIENT_ID)

    private static final String ACCESS_TOKEN_NAME = "webapi.credentials.access_token";
    private static final String ACCESS_TOKEN = "access_token";
    private static final String EXPIRES_AT = "expires_at";

    private static final String USER_SPOTIFY = "user_spotify";

    public SpotifyCredentials() {
        setSharedPreferences();
    }

    private void setSharedPreferences() {
        preferences = getSharedPreferences(Application.getInstance());
        editor = preferences.edit();
    }

    private SharedPreferences getSharedPreferences(Context appContext) {
        return appContext.getSharedPreferences(ACCESS_TOKEN_NAME, Context.MODE_PRIVATE);
    }

    public void cleanPreferences(){
        editor.clear();
        editor.commit();
    }

    public void setToken(Context context, String token, long expiresIn, TimeUnit unit) {

        long now = System.currentTimeMillis();
        long expiresAt = now + unit.toMillis(expiresIn);

        editor.putString(ACCESS_TOKEN, token);
        editor.putLong(EXPIRES_AT, expiresAt);
        editor.apply();
    }

    public String getToken() {

        String token = preferences.getString(ACCESS_TOKEN, null);
        long expiresAt = preferences.getLong(EXPIRES_AT, 0L);

        if (token == null || expiresAt < System.currentTimeMillis()) {
            return null;
        }

        return token;
    }

    public void setSpotifyUser(String user){
        editor.putString(USER_SPOTIFY, user);
        editor.commit();
    }

    public String getUserSpotify(){
        return preferences.getString(USER_SPOTIFY, "");
    }

}
