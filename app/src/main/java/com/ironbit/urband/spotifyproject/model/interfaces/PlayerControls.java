package com.ironbit.urband.spotifyproject.model.interfaces;

import android.support.annotation.Nullable;

/**
 * Created by Alejandro on 04/03/2016.
 */
public interface PlayerControls {

    void play(String url);

    void pause();

    void resume();

    boolean isPlaying();

    @Nullable
    String getCurrentTrack();

    void release();
}