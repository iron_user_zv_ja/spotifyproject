package com.ironbit.urband.spotifyproject.presenter.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.ironbit.urband.spotifyproject.Application;
import com.ironbit.urband.spotifyproject.R;
import com.ironbit.urband.spotifyproject.model.interfaces.SpotifyControls;
import com.ironbit.urband.spotifyproject.view.adapters.RecyclerPlaylistSongs;

import java.util.ArrayList;
import java.util.List;

import kaaes.spotify.webapi.android.models.PlaylistSimple;
import kaaes.spotify.webapi.android.models.Track;

/**
 * Created by Alejandro on 10/02/2016.
 */

public class HomeMusicSpotifySongs extends AppCompatActivity implements View.OnClickListener, SpotifyControls.AddItem {

    private RecyclerView recyclerSongs;
    public RecyclerPlaylistSongs adapterSongs;
    private LinearLayoutManager llmSongs;

    private Button btn_next, btn_previous;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity_music_spotify_songs);

        initResources();

        Bundle args = getIntent().getExtras();
        if (args != null) {
            args.getString("idPlaylist");
            Application.getInstance().getActionListener().getListSongs(args.getString("idPlaylist"));
        }

        setSpotifyAdapter();
    }

    private void initResources() {

        btn_next = (Button) findViewById(R.id.music_spotify_btn_skiptonext);
        btn_previous = (Button) findViewById(R.id.music_spotify_btn_skiptoprevious);
        btn_next.setOnClickListener(this);
        btn_previous.setOnClickListener(this);

        recyclerSongs = (RecyclerView) findViewById(R.id.music_spotify_list_songs);
        recyclerSongs.setHasFixedSize(true);

        llmSongs = new LinearLayoutManager(this);
        llmSongs.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerSongs.setLayoutManager(llmSongs);

        Application.getInstance().getActionListener().setSpotifyControlsAddItem(this);
    }

    private void setSpotifyAdapter() {

        adapterSongs = new RecyclerPlaylistSongs(this, R.layout.home_fragment_music_spotify_playlists_row);
        adapterSongs.setOnItemRecyclerClickListener(new RecyclerPlaylistSongs.OnItemRecyclerClickListener() {
            @Override
            public void onItemRecyclerClick(View view, Track item, List<Track> items, int position) {

                List<String> itemsUri = new ArrayList<String>();

                for (Track track : items) {
                    itemsUri.add(track.uri);
                }
                for (int i = 0; i < position; i++) {
                    itemsUri.add(items.get(i).uri);
                }
                for (int i = 0; i < position; i++) {
                    itemsUri.remove(0);
                }
/*
                for (String name : itemsUri){
                    Log.i("SONGS: debug: ", "song: " + name);
                }
*/
                Application.getInstance().getActionListener().selectTrack(item, itemsUri);
            }

        });

        recyclerSongs.setAdapter(adapterSongs);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.music_spotify_btn_skiptonext:
                Application.getInstance().getActionListener().skipToNextTrack();
                break;
            case R.id.music_spotify_btn_skiptoprevious:
                Application.getInstance().getActionListener().skipToPreviousTrack();
                break;

        }
    }

    @Override
    public void reset() {
        adapterSongs.clearDataSpotify();
    }

    @Override
    public void addData(List<Track> items) {
        adapterSongs.addDataSpotify(items);
    }

}
