package com.ironbit.urband.spotifyproject.presenter.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.ironbit.urband.spotifyproject.Application;
import com.ironbit.urband.spotifyproject.model.interfaces.PlayerControls;
import com.ironbit.urband.spotifyproject.model.interfaces.SpotifyControls;
import com.ironbit.urband.spotifyproject.model.interfaces.SpotifyListener;
import com.ironbit.urband.spotifyproject.view.utils.PreviewPlayer;
import com.spotify.sdk.android.player.ConnectionStateCallback;
import com.spotify.sdk.android.player.PlayerNotificationCallback;
import com.spotify.sdk.android.player.PlayerState;
import com.spotify.sdk.android.player.PlayerStateCallback;

import java.util.List;

import kaaes.spotify.webapi.android.models.PlaylistSimple;
import kaaes.spotify.webapi.android.models.Track;

/**
 * Created by Alejandro on 04/03/2016.
 */
public class PlayerService extends Service implements SpotifyControls.ActionListener, PlayerNotificationCallback, ConnectionStateCallback {

    //private final IBinder mBinder = new PlayerBinder();
    //private PreviewPlayer mPlayer = new PreviewPlayer();

    private SpotifyControls.AddPlaylist mAddPlaylist;
    private SpotifyControls.AddItem mAddItem;
    private SpotifyControls.AddUser mAddUser;

    private SpotifyListener.CompleteListenerUser listenerUser;
    private SpotifyListener.CompleteListenerListSongs listenerSongs;
    private SpotifyListener.CompleteListenerPlayList listenerPlaylists;

    private String currentTrack = "";
    private boolean playerIsPlaying = false;

    public static Intent getIntent(Context context) {
        return new Intent(context, PlayerService.class);
    }

    /*
    public class PlayerBinder extends Binder {
        public PlayerControls getService() {
            return mPlayer;
        }
    }
    */

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        //mPlayer.release();
        super.onDestroy();
    }

    @Override
    public void getUser() {
        listenerUser = new SpotifyListener.CompleteListenerUser(){

            @Override
            public void onComplete(String user) {
                mAddUser.addUser(user);
            }

            @Override
            public void onError(Throwable error) {

            }
        };
        Application.getInstance().getSpotifyCallbackUtils().getNameUser(listenerUser);
    }

    @Override
    public void getPlaylists() {
        listenerPlaylists = new SpotifyListener.CompleteListenerPlayList(){
            @Override
            public void onComplete(List<PlaylistSimple> items) {
                mAddPlaylist.addPlayList(items);
            }

            @Override
            public void onError(Throwable error) {

            }
        };
        Application.getInstance().getSpotifyCallbackUtils().getPlaylists(listenerPlaylists);
    }

    @Override
    public void getListSongs(String idPlaylist) {
        listenerSongs = new SpotifyListener.CompleteListenerListSongs() {
            @Override
            public void onComplete(List<Track> items) {
                mAddItem.addData(items);
            }

            @Override
            public void onError(Throwable error) {
                //logError(error.getMessage());
                Log.i("SpotifyHelper", error.getMessage());
            }
        };
        Application.getInstance().getSpotifyCallbackUtils().getListSongs(listenerSongs, idPlaylist);
    }

    @Override
    public void setSpotifyControlsAddPlaylist(SpotifyControls.AddPlaylist addPlaylist) {
        mAddPlaylist = addPlaylist;
    }

    @Override
    public void setSpotifyControlsAddItem(SpotifyControls.AddItem addItem) {
        mAddItem = addItem;
    }

    @Override
    public void setSpotifyControlsAddUser(SpotifyControls.AddUser addUser) {
        mAddUser = addUser;
    }

    @Override
    public void init(String token) {

    }

    @Override
    public void selectTrack(final Track item, final List<String> uris) {

        final String previewUrl = item.uri;

        Application.getInstance().getPlayerSpotify().getPlayerState(new PlayerStateCallback() {
            @Override
            public void onPlayerState(PlayerState playerState) {

                currentTrack = playerState.trackUri;
                playerIsPlaying = playerState.playing;

                if (item.uri == null) {
                    Log.i("debug:", "Track doesn't have a uri correct");
                    return;
                }

                if (Application.getInstance().getPlayerSpotify() == null) return;

                if (currentTrack == null || !currentTrack.equals(previewUrl)) {
                    //mPlayer.play(previewUrl);
                    Application.getInstance().getPlayerSpotify().play(uris);
                } else if (playerIsPlaying) {
                    Application.getInstance().getPlayerSpotify().pause();
                } else {
                    Application.getInstance().getPlayerSpotify().resume();
                }

            }
        });
    }

    @Override
    public void skipToNextTrack() {

    }

    @Override
    public void skipToPreviousTrack() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onLoggedIn() {

    }

    @Override
    public void onLoggedOut() {

    }

    @Override
    public void onLoginFailed(Throwable throwable) {

    }

    @Override
    public void onTemporaryError() {

    }

    @Override
    public void onConnectionMessage(String s) {

    }

    @Override
    public void onPlaybackEvent(EventType eventType, PlayerState playerState) {

    }

    @Override
    public void onPlaybackError(ErrorType errorType, String s) {

    }


}
